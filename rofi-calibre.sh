#!/bin/bash

# rofi -modi "calibre:rofi-calibre.sh" -show calibre

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

echo -en "\0no-custom\x1ftrue\n"

if [[ $ROFI_RETV -eq 1 ]]
then
	# $info[0] == Action
	# $info[1] == ID
	# $info[2] == Data
	# $info[3] == Title
	IFS='|'; info=($ROFI_INFO); unset IFS;
	echo -en "\0prompt\x1f" ${info[3]} "\n"
fi

if [[ $ROFI_RETV -eq 0 ]]
then
	echo -en "\0prompt\x1fCalibre\n"
	# Assumes | (pipe) is not used anywhere in the metadata
	calibredb list -f 'id,authors,title,rating,*reading,*finished,*priority' --separator '|' -w 10000 |& \
		gawk -F '|' -f "${SCRIPTPATH}/booklist.awk"
fi

if [[ ${info[0]} = 'manage' ]]
then
	calibredb list -f formats --for-machine -s id:${info[1]} | \
		gawk -F '"' \
			 'BEGIN { in_formats = 0 }
			  $2 == "formats" && $3 ~ /:/ {
			 		in_formats = 1
					next
			  }
			  in_formats == 1 && $1 ~ /],/ {
			  		in_formats = 0
					next
			  }
			  /"/ && in_formats == 1 {
					match($2, /\.[A-Za-z0-9]+$/, formats)
				  	printf "%s \0info\x1fopen|%s\n", formats[0], $2
			  }'

	echo -en "--- \0nonselectable\x1ftrue\n"

	echo -en "Reading \0info\x1freading|${info[1]}|-|${info[3]}\n"
	echo -en "Finished \0info\x1ffinished|${info[1]}|-|${info[3]}\n"
	echo -en "Rating \0info\x1frating|${info[1]}|-|${info[3]}\n"
	echo -en "Priority \0info\x1fpriority|${info[1]}|-|${info[3]}\n"
fi

if [[ ${info[0]} = 'reading' ]]
then
	if [[ ${info[2]} = 'yes' ]]
	then
		coproc ( calibredb set_custom reading ${info[1]} True  > /dev/null  2>1 )
	elif [[ ${info[2]} = 'no' ]]
	then
		coproc ( calibredb set_custom reading ${info[1]} None  > /dev/null  2>1 )
	else
		echo -en "Reading: \0info\x1fmanage|${info[1]}|-|${info[3]}\n"
		echo -en "Yes \0info\x1freading|${info[1]}|yes|-\n"
		echo -en "No \0info\x1freading|${info[1]}|no|-\n"
	fi
fi

if [[ ${info[0]} = 'finished' ]]
then
	if [[ ${info[2]} = 'yes' ]]
	then
		coproc ( calibredb set_custom finished ${info[1]} True  > /dev/null  2>1 )
	elif [[ ${info[2]} = 'no' ]]
	then
		coproc ( calibredb set_custom finished ${info[1]} None  > /dev/null  2>1 )
	else
		echo -en "Finished: \0info\x1fmanage|${info[1]}|-|${info[3]}\n"
		echo -en "Yes \0info\x1ffinished|${info[1]}|yes|-\n"
		echo -en "No \0info\x1ffinished|${info[1]}|no|-\n"
	fi
fi

if [[ ${info[0]} = 'rating' ]]
then
	if [[ ${info[2]} =~ ^[0-9]$ ]]
	then
		coproc ( calibredb set_metadata -f rating:${info[2]} ${info[1]}  > /dev/null  2>1 )
	else
		echo -en "Rating: \0info\x1fmanage|${info[1]}|-|${info[3]}\n"
		echo -en "0 Not Rated \0info\x1frating|${info[1]}|0|-\n"
		echo -en "1 ★☆☆☆☆ \0info\x1frating|${info[1]}|1|-\n"
		echo -en "2 ★★☆☆☆ \0info\x1frating|${info[1]}|2|-\n"
		echo -en "3 ★★★☆☆ \0info\x1frating|${info[1]}|3|-\n"
		echo -en "4 ★★★★☆ \0info\x1frating|${info[1]}|4|-\n"
		echo -en "5 ★★★★★ \0info\x1frating|${info[1]}|5|-\n"
	fi
fi

if [[ ${info[0]} = 'priority' ]]
then
	if [[ ${info[2]} =~ ^[0-9]$ ]]
	then
		real_priority=$(( ${info[2]} * 2 ))
		coproc ( calibredb set_custom priority ${info[1]} $real_priority  > /dev/null  2>1 )
	else
		echo -en "Priority: \0info\x1fmanage|${info[1]}|-|${info[3]}\n"
		echo -en "0 No Priority \0info\x1fpriority|${info[1]}|0|-\n"
		echo -en "1 ★☆☆☆☆ \0info\x1fpriority|${info[1]}|1|-\n"
		echo -en "2 ★★☆☆☆ \0info\x1fpriority|${info[1]}|2|-\n"
		echo -en "3 ★★★☆☆ \0info\x1fpriority|${info[1]}|3|-\n"
		echo -en "4 ★★★★☆ \0info\x1fpriority|${info[1]}|4|-\n"
		echo -en "5 ★★★★★ \0info\x1fpriority|${info[1]}|5|-\n"
	fi
fi

if [[ ${info[0]} = 'open' ]]
then
	coproc ( xdg-open "${info[1]}"  > /dev/null  2>1 )
fi

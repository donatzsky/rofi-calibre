{
	# Table headers
	if ($1 ~ /^id *$/) {
		next
	}
}

{
	# Anything not a book entry
	if ($1 !~ /^[0-9]+ *$/) {
		print $0
		next
	}
}

{
	if ($1 ~ /^[0-9]+ *$/) {
		id = $1
		authors = $2
		title = $3

		if ($4 == "None" || $4 == 0) {
			rating = "  "
		}
		else {
			rating = "*" ($4 / 2)
		}

		if ($5 == "True") {
			reading = "^"
			readingsort = "a"
		}
		else {
			reading = " "
			readingsort = "b"
		}

		if ($6 == "True") {
			finished = "="
		}
		else {
			finished = " "
		}

		if ($7 == "None" || $7 == 0 || $7 == "") {
			priority = "  "
			prioritysort = 5
		}
		else {
			priority = "+" ($7 / 2)
			prioritysort = (5 - ($7 / 2))
		}

		sortstring = readingsort " " prioritysort " " authors " " title

		booklist[sortstring] = sprintf("%.20s  %s %s %s %s  %s \0meta\x1f%s\x1finfo\x1fmanage|%i|-|%s\n", authors, reading, finished, rating, priority, title, authors, id, title)
	}
}

END {
	n = asorti(booklist, sortkey)
	for (i = 1; i <= n; i++) {
		print booklist[sortkey[i]]
	}
}
